#include <stdio.h>
#include <stdlib.h>

int main() {
    int n;
    int i;
    int *punterov;
    printf("Ingrese el tamaño del vector: ");
    scanf("%d", &n);
    int v[n];
    printf("\nIngrese los valores del vector:\n");
    for (i = 0; i < n; i++) {
        printf("posicion %d: ", i + 1);
        scanf("%d", &v[i]);
    }
    punterov = &v;

    printf("\nVector de la manera ingresada:\n[");
    for (i = 0; i < n; i++) {
        printf(" %d ", *punterov);
        puntero++;
    }
    printf("]\n");

    for (i = 0; i<n; i++) {
        printf("Dirección %d : %p\n", *punterov, punterov);
    }
    return 0;
}
